module.exports.PORT = process.env.PORT || 3000;
module.exports.ENV = process.env.NODE_ENV || 'development';
module.exports.originsAlloweds = [
    "http://127.0.0.1:8888", 
    "http://192.168.41.16", 
    "https://www.credbell.com.br", 
    "https://*.credbell.com.br",
    "https://*.redeconekta.com.br", 
    "https://*.globelltec.net.br"
];