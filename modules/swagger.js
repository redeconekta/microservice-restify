var restifySwaggerJsdoc = require('restify-swagger-jsdoc');
module.exports.swagger =  function(server){
    restifySwaggerJsdoc.createSwaggerPage({
        title: global.projectPackage.name, 
        version: global.projectPackage.version,
        server: server,
        path: "/docs",
        apis: ['./docs/*']
    });
}