module.exports.getPath = function (yourPath){
    var path = __dirname.slice(0, __dirname.indexOf("node_modules"));
    if (yourPath){
        path = require("path").join(path, `./${yourPath}`);
    } 
    return path;
}