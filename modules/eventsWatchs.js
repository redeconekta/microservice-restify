const log = require("./winston.js")();

module.exports = function (server, configs){
    server.pre(function writeHttpLog(request, response, next){
        var body = request.body;
        body = body && typeof body == "string" ? JSON.parse(body) : body;
        body = body && Object.keys(body).length ? JSON.stringify(body) : "";
        log.info(`(${request.reqId}) RECV ${request.method} ${request.url} 
                      H: ${JSON.stringify(request.headers)}${getBodyFormattedToLog(body)}`);
        return next();
    });

    server.on('after', function (request, response){
        log.info(`(${request.reqId}) SEND HTTP ${response.statusCode} ${response.statusMessage} ${getBodyFormattedToLog(response._data)}`);
    });
    
    server.on("NotFound", function (request, response){
        response.status(404)
        response.send({
            error: "Route",
            code: 404,
            message: `The route ${response.req.url} is not found`
        });
    });
    
    process.on('SIGINT', function () {
        log.info(`Stopped ${configs.name} ${configs.version} (${configs.ENV}) on port ${configs.PORT}.`);
        process.exit(1);
    });
}

function getBodyFormattedToLog(body){
    return body ? `\n\t\t\t\t\t  B: ${body}` : "";
}