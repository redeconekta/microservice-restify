const fs = require('fs');
const moment = require('moment-timezone');
const winston = require('winston');
require('winston-daily-rotate-file');
const { combine, printf } = winston.format;

var transport = new (winston.transports.DailyRotateFile)({
    filename: `${global.projectPackage.name}-%DATE%.log`,
    datePattern: 'YYYYMMDD',
    dirname: "./logs",
    zippedArchive: false,
    maxFiles: '60d'
});

module.exports = function (){
    var logDir = './logs';
    if (!fs.existsSync(logDir)) {
        fs.mkdirSync(logDir);
    }

    const myFormat = printf(function (info){
            return `[${info.timestamp}] ${info.message}`;
    });

    const appendTimestamp = winston.format(function (info){
        info.timestamp = moment().tz("America/Sao_Paulo").format("YYYY-MM-DD HH:mm:ss");
        return info;
    });

    const logger = winston.createLogger({
        format: combine(
            appendTimestamp(),
            myFormat
        ),
        transports: [
            transport
        ],
        exitOnError: false
    });
    return logger;
}