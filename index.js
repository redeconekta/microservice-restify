const restify = require("restify");
const fs = require("fs");
const corsMiddleware = require("restify-cors-middleware");
const restfiyXRequestId = require("restify-x-request-id").default;
const project = require("./modules/project.js");
global.projectPackage = JSON.parse(fs.readFileSync(project.getPath() + 'package.json').toString());
const log = require("./modules/winston.js")();
const serverConfig = require("./config/server.js");
const server = restify.createServer();
const cors = corsMiddleware({
    origins: serverConfig.originsAlloweds,
    allowHeaders: ['*'],
    exposeHeaders: ['*']
});

server.pre(restfiyXRequestId.middleware);
server.pre(cors.preflight);
server.pre(restify.plugins.bodyParser());
server.pre(restify.plugins.queryParser());
server.pre(restify.plugins.pre.context());
server.pre(function writeRequestLog(request, response, next){
    request.logInfo = function(message){
        log.info(`(${request.reqId}) ${message}`);
    }
    return next();
});
server.use(cors.actual);

function setRoutesInServer(projectPath = '') {
	if (projectPath == ''){
		projectPath = project.getPath("routes");
		if (!fs.existsSync(projectPath)) {
			projectPath = project.getPath("src/routes");
		}
	}

	fs.readdirSync(projectPath).forEach(function(file) {
		if (fs.statSync(projectPath + '/' + file).isDirectory()){
			setRoutesInServer(projectPath + '/' + file);
		} else{
			require(`${projectPath}/${file}`)(server);
		}
	});
}

setRoutesInServer();
require("./modules/eventsWatchs.js")(server, Object.assign(global.projectPackage, serverConfig));
require("./modules/swagger.js").swagger(server);

function createServer(){
    server.listen(serverConfig.PORT, function (){
        log.info(`Started ${global.projectPackage.name} ${global.projectPackage.version} (${serverConfig.ENV}) on port ${serverConfig.PORT}, ready to go!`);
    });
    return server;
}

function insertFunctionInServer(keyServer, functionToInsert, insertBefore = false){
	if(insertBefore){
		let chain = `${keyServer}Chain`;
		let indexStack = server[chain]._stack.findIndex(function(item){
			return item.name == insertBefore;
		});
		server[chain]._stack.splice(indexStack, 0, functionToInsert);
	} else {
		server[keyServer](functionToInsert);
	}
}

function serverLog(message){
    log.info(`${message}`);
}

module.exports.createServer = createServer;
module.exports.log = serverLog;
module.exports.package = global.projectPackage;
module.exports.insertFunctionInServer = insertFunctionInServer;